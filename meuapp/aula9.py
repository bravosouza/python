import shutil

def escrever_arquivo(texto):
    arquivo = open('teste2.txt', 'w') #cria um arquivo com o nome 'teste, o w serve para escrever'
    arquivo.write(texto) # write sempre realiza uma nova escrita, se já existir ele subistitui o que está escrito
    arquivo.close() #sai do arquivo

def atualizar_arquivo(texto):
    arquivo = open('teste2.txt','a')# o a verifica se já existe o arquivo criado e se existe ele atualiza sem substituir informações
    arquivo.write(texto)# \n para pular linha
    arquivo.close()

def ler_arquivo(nome_arquivo):
    arquivo = open(nome_arquivo, 'r')# o R server para leitura de arquivos.
    texto = arquivo.read()
    print(texto)

#Também podemos gerar ou ler algum arquivo que está fora do projeto.

def arquivo_externo(texto):
    diretorio = '/home/msouza/PycharmProjects/python/meuapp/arquivos/texte1.txt'
    arquivo = open(diretorio, 'w') #cria um arquivo com o nome 'teste, o w serve para escrever'
    arquivo.write(texto) # write sempre realiza uma nova escrita, se já existir ele subistitui o que está escrito
    arquivo.close() #sai do arquivo

def copia_arquivo(nome_arquivo): #copia o arquivo
    shutil.copy(nome_arquivo, '/home/msouza/PycharmProjects/python/meuapp/copia_teste.txt')

def move_arquivo(nome_arquivo): #move o arquivo para outro diretório
    shutil.move(nome_arquivo, '/home/msouza/PycharmProjects/python/meuapp/arquivos/')

if __name__ == '__main__':
   # escrever_arquivo('Primeira linha.\n')
   #atualizar_arquivo('Segunda linha.\n')
   #ler_arquivo('teste2.txt')
   #arquivo_externo('Primeira linha.\n')
   #copia_arquivo('teste.txt')
   move_arquivo('texte.txt')