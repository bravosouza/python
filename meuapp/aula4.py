# #conta até 100 e imprime o número percorrido.
 for x in range(101):
     print(x)

# #Receber um valor e validar se é primo ou não
 a = int(input('Entre com o número: '))
 div = 0
 for x in range(1, a+1):
     resto = a % x
     print(x, resto)
     if resto == 0:
         div += 1 #soma + 1 em div
 if div == 2:
     print('Número {} é primo'.format(a))
 else:
     print('Número {} não é primo'.format(a))

#Imprimir apenas números primos de 0 à 100
 for num in range(101):
     div = 0
     for x in range(1, num+1):
         resto = num % x
         if resto == 0:
             div += 1
     if div == 2:
         print(num)

#Receber um valor e percorrer os números primos até ele
 a = int(input('Entre com um valor: '))
 for num in range(a+1):
     div = 0
     for x in range(1, num+1):
         resto = num % x
         if resto == 0:
             div += 1
     if div == 2:
         print(num)

#Forçar uma repetição até que a condição seja verdadeira
 a = 0
 while a <= 10:
     print(a)
     a += 1

#solicitar uma nota de 0 à 10 e repetir a mensagem até que a nota esteja correta
 nota = int(input('Entre com a nota: '))
 while nota > 10:
    nota = int(input('Nota inválida. Entre com a nota correta: '))

# Testando a média com while
a = int(input('Primeiro bimestre: '))
while a > 10:
    a = int(input('Nota inválida! Primeiro bimestre: '))
b = int(input('Segundo bimestre: '))
while b > 10:
    b = int(input('Nota inválida! Segundo bimestre: '))
c = int(input('Terceiro bimestre: '))
while c > 10:
    c = int(input('Nota inválida! Terceiro bimestre: '))
d = int(input('Quarto bimestre: '))
while d > 10:
    d = int(input('Nota inválida! Quarto bimestre'))
media = (a + b + c + d) / 4
print('A média é: {}'.format(media))