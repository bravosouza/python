#criando um conjunto
conjunto = {1, 2, 3, 4}
print(type(conjunto))
print(conjunto)
#conjunto não contem duplicidade

conjunto.add(5) #adiciona um elemento ao conjunto
conjunto.discard(2) #remove um elemento do conjunto

conjunto = {1, 2, 3, 4, 5}
conjunto2 = {5, 6, 7, 8}

conjunto_uniao = conjunto.union(conjunto2) #unindo os dois conjuntos.
print('União: {}'.format(conjunto_uniao))

conjunto_interseccao = conjunto.intersection(conjunto2) #captura tudo que contem nos dois conjuntos
print('Intersecção: {}'.format(conjunto_interseccao))

conjunto_diferenca1 = conjunto.difference(conjunto2) #captura tudo que contem de diferente nos conjuntos
conjunto_diferenca2 = conjunto2.difference(conjunto)
print('Diferença entre 1 e 2: {}'.format(conjunto_diferenca1))
print('Diferença entre 2 e 1: {}'.format(conjunto_diferenca2))

conjunto_diff_simetrica = conjunto.symmetric_difference(conjunto2) #tudo que tem de diferente nos dois conjuntos
print('Diferença simétrica: {}'.format(conjunto_diff_simetrica))

conjunto_a = {1, 2, 3}
conjunto_b = {1, 2, 3, 4, 5}
conjunto_subset = conjunto_a.issubset(conjunto_b) #verifica se um conjunto é subconjunto de outro
conjunto_subset1 = conjunto_b.issubset(conjunto_a)
print('A é subconjunto de B? {}'.format(conjunto_subset))
print('B é subconjunto de A? {}'.format(conjunto_subset1))

conjunto_superset = conjunto_b.issuperset(conjunto_a) #verifica se um conjunto é superconjunto de outro
print('B é um superconjunto de A? {}'.format(conjunto_superset))

lista = ['cachorro', 'cachorro', 'Gato', 'Gato', 'Elefante']
print(lista)
conjunto_animais = set(lista) #converte uma lista em um conjunto
print(conjunto_animais)
lista_animais = list(conjunto_animais) #converte um conjunto em uma lista
print(lista_animais)