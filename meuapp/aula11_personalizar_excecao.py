class Error(Exception): # classes personalizada de erro.
    pass

class InputError(Error):
    def __init__(self, message):
        self.message = message

while True: #enquanto verdade for verdade
    try:
        x = int(input('Entre com uma nota de 0 a 10:'))
        print(x)

        if x > 10:
            raise InputError('Nota não pode ser maior que 10.')  #força uma exceção
        elif x < 0:
            raise InputError('Nota nao pode ser menor que 0.')

        break #saida do loop

    except ValueError:
        print('Valor inválido. De   ve-se digitar apenas números.')

    except InputError as ex:
        print(ex)
