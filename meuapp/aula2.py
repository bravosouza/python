a = 10
b = 5
soma = a + b
subtracao = a - b
multiplicacao = a * b
divisao = a / b
resto = a % b
print(type(soma)) # imprime o tipo da variavel
soma = str(soma) #converte variavel em string
print (type(soma))
print('soma:'+ str(soma)) #concatenando um texto com uma string
print('soma: {}'.format(soma)) #concatena qualquer tipo de varival a um texto
print('Soma: {} . Subtracao: {}'.format(soma,subtracao)) # mais de uma variavel no mesmo print
print('Soma: {soma} . Subtracao: {subtracao}'. format(soma=soma, subtracao=subtracao)) #declarando valores na impressão
print('Soma: {soma}.'
      '\nSubtração: {subtracao}'
      '\nDivisão: {divisao}'
      '\nResto: {resto}'
      .format(soma=soma, subtracao=subtracao, divisao=divisao, resto=resto)) #imprime cada variavel em um linha com \n

print (int(divisao)) #arredonda o valor do resultado
x = '1'
soma2 = int(x) + 1 #converte X em inteiro
print (soma2)

#interação do usuário:

c = int(input('Entre com o primeiro valor: ')) #recebe um valor inteiro digitado pelo usuário
d = int(input('Entre com o segundo valor: '))
resposta = c + d
resultado = ('Resultado: {resposta}'.format(resposta=resposta))
print (resultado)