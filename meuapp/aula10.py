#Trazer data date

from datetime import date, time, datetime, timedelta

def trabalhando_com_date():
    data_atual = date.today() #data atual
    print(data_atual.strftime('%d/%m/%Y')) #converte para string no formato informado
    print(data_atual.strftime('%A %B %Y'))
    print(type(data_atual))

    data_atual_str = data_atual.strftime('%A %B %Y')
    print(data_atual_str)
    print(type(data_atual_str))

#Trazer hora time

def trabalhando_com_time():
    horario = time(hour=15, minute=18, second=30) #hora, minuto e segundo
    print(type(horario))#tipo da variavel
    print(horario) #valor da variavel

    horario_str = horario.strftime('%H:%M:%S') #converte para string no formato informado
    print(type(horario_str))
    print(horario_str)

#Trazer data e hora datetime

def trabalhando_com_datetime():
    data_atual = datetime.now()
    print(data_atual)
    print(data_atual.strftime('%d/%m/%y %H:%M:%S')) #converte para string no formato informado
    print(data_atual.strftime('%c')) #dia e mês com datetime
    print(data_atual.day) #traz o dia, existe várias funções que já tras a informação, sempre chamada em inglês
    print(data_atual.weekday()) #dias da semana

    #dias da semana em português:
    tupla = ('Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo')
    print(tupla[data_atual.weekday()]) #pega o dado da tupla de acordo com o dia da semana que estamos, também é possivel realizar essa operação com o mês.

    #criar uma data aleatória
    data_criada = datetime(2018, 6, 20, 15, 30, 20)
    print(data_criada)
    print(data_criada.strftime('%c'))#converter em string para o fomato informado

    #converter string em datetime
    data_string = '01/10/2019 12:20:22'
    data_convertida = datetime.strptime(data_string,'%d/%m/%Y %H:%M:%S')
    print(data_convertida)

# Realizar operações com data e hora timedelta
    nova_data = data_convertida - timedelta(days=365, hours=2, minutes=15) #remover um ano, horas e minutos da data atual, podemos sumar ou subtrair.
    print(nova_data)

if __name__ == '__main__':
   # trabalhando_com_date()
   #trabalhando_com_time()
    trabalhando_com_datetime()