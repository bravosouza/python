#importanto o pacote instalado
import requests

def retorna_dados_cep(cep):
    response = requests.get('https://viacep.com.br/ws/{}/json/'.format(cep))
    print(response.status_code)
    print(response.text) #traz o texto em formato de json

    print(response.json()) #trazendo em formato de dicionário

    dados_cep = response.json() #recebe o dicionário
    print(dados_cep['logradouro']) #imprimindo apenas um dado do dicionário
    print(dados_cep['cep'])

def retorna_dados_pokemon(pokemon):
    response = requests.get('https://pokeapi.co/api/v2/pokemon/{}/'.format(pokemon))
    dados_pokemon = response.json()
    return dados_pokemon

def retorna_response(url):
    response = requests.get(url)
    return response.text

if __name__ == '__main__':
    #retorna_dados_cep('22041001')
    #dados_pokemon = retorna_dados_pokemon('pikachu')
    #print(dados_pokemon['sprites']['front_shiny']) #chamando dados do dicionário
    response = retorna_response('https://globallabs.academy/') #realiza a leitura da página e retorna o html
    print(response)