def atualizar_notas(nome_arquivo, texto): #irá receber o nome do arquivo e o texto
    arquivo = open(nome_arquivo,'a')# o a verifica se já existe o arquivo criado e se existe ele atualiza sem substituir informações
    arquivo.write(texto)# \n para pular linha
    arquivo.close()

def ler_notas(nome_arquivo):
    arquivo = open(nome_arquivo, 'r')# o R server para leitura de arquivos.
    texto = arquivo.read()
    print(texto)

def media_notas(nome_arquivo):
    global aluno
    arquivo = open(nome_arquivo, 'r')
    aluno_nota = arquivo.read()
    #print(aluno_nota)
    aluno_nota = aluno_nota.split('\n') # sempre que encontra um enter(quebra de linha),pega o item inteiro e coloca na lista
    #print(aluno_nota)
    lista_media = []
    for x in aluno_nota:
        lista_notas = x.split(',')#quebrando arquivo pela virgula, separando alunos de notas.
        aluno = lista_notas[0]
        lista_notas.pop(0) #converte as strings
        print(aluno)
        print(lista_notas)
        media = lambda notas: sum([int(i) for i in notas]) / 4 #converte a lista em inteiro e calcula a média, 4 é o total da lista
        print(media(lista_notas))
        lista_media.append({aluno:media(lista_notas)})
    return lista_media

if __name__ == '__main__':
    lista_media = media_notas('notas.txt')
    print(lista_media)
    #media_notas('notas.txt')
    #aluno = 'Cesar,7,9,3,8\n'
    atualizar_notas('notas.txt', aluno)
    #ler_notas('notas.txt')
