# criando um arquivo

arquivo = open('teste.txt', 'w') #cria um arquivo com o nome 'teste, o w serve para escrever'
arquivo.write('Minha primeira escrita') # write sempre realiza uma nova escrita, se já existir ele subistitui o que está escrito
arquivo.close() #sai do arquivo

arquivo = open('teste.txt','a')# o a verifica se já existe o arquivo criado e se existe ele atualiza sem substituir informações
arquivo.write('\nSegunda linha')# \n para pular linha
arquivo.close()

