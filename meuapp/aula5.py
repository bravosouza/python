# Criando uma lista4

lista = [12, 10, 5, 7]
lista_animal = ['cachorro', 'gato', 'elefante', 'lobo', 'arara']
print(lista_animal[1]) #Imprime um elemento da posição 1

for x in lista_animal:
    print(x)  #pecorre todos os elementos da lista

 soma = 0
 for x in lista:
     print(x)
     soma += x
 print(soma) #soma os elementos da lista.

 print(sum(lista)) #soma os elementos da lista desde que a lista toda seja do mesmo tipo
 print(max(lista)) #Busca o maior valor da lista
 print(min(lista)) #Busca o menor valor da lista

 nova_lista = lista_animal * 3 # multiplica dos valores de uma lista
 print(nova_lista)

 if 'gato' in lista_animal: #Valida se existe esse elemento na lista
     print('existe um gato na lista')
 else:
     print('não existe um gato na lista')

 if 'lobo' in lista_animal:
     print('existe um lobo na lista')
 else:
     print('não existe um lobo na lista. Será incluído.')
     lista_animal.append('lobo') #inclui um elemento na lista
     print(lista_animal)

 lista_animal.pop() #retira a ultima posição da lista
 lista_animal.pop(0) #retira o elemento da posição 0
 lista_animal.remove('elefante') #remove o elefante da lista

 lista.sort() #ordena os elementos de uma lista
lista_animal.sort()
print(lista)
print(lista_animal)

lista_animal.reverse() #ordena de forma reversa, do fim para o começo
print(lista_animal)

#lista é mutavel, tupla é imuntavel ou seja não pe possivel alterar um objeto

#Declarando uma tupla:

tupla = (1, 10, 12, 14)
print(tupla[1])
print(len(tupla)) #retorna a quantidade de elementos
print(len(lista_animal))

tupla_animal = tuple(lista_animal) #converte uma lista em uma tupla
print(type(tupla_animal))
print(tupla_animal)

lista_numerica = list(tupla) #converte uma tupla em uma lista
print(type(lista_numerica))
print(lista_numerica)