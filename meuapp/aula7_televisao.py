class Televisao:
    def __init__(self):
        self.ligada = False

    def power(self):
        if self.ligada: #já verifica se é verdadeiro sem mostrar o valor.
            self.ligada = False
            self.canal = 5
        else:
            self.ligada = True

    def aumenta_canal(self):
        if self.ligada:
            self.canal += 1

    def diminui_canal(self):
        if self.ligada:
            self.canal -= 1

if __name__== '__main__': #só imprime se a chamada do múduloestiver no mesmo arquivo, so for importado no console não imprime nada.
    televisao = Televisao()
    print('Televisão está ligada? {}'.format(televisao.ligada))
    televisao.power()
    print('Televisão está ligada? {}'.format(televisao.ligada))
    televisao.power()
    print('Televisão está ligada? {}'.format(televisao.ligada))
    print('Canal: {}'.format(televisao.canal))
    televisao.power()
    televisao.aumenta_canal()
    print('Televisão está ligada? {}'.format(televisao.ligada))
    print('Canal: {}'.format(televisao.canal))
    televisao.aumenta_canal()
    print('Canal: {}'.format(televisao.canal))
    televisao.diminui_canal()
    print('Canal: {}'.format(televisao.canal))