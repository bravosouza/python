
lista = [1,10]

try: #tudo que estiver dentro, faz parte da exceção
    arquivo = open('texte.txt', 'r')
    texto = arquivo.read()
    divisao = 10 / 1
    numero = lista[1]

except ZeroDivisionError: #classe de exceções
    print('Não é possivel realizar uma divisão por 0')

#except: #tratamento de erro genérico
 #   print('Erro desconhecido')

except IndexError: #encadeamento de exceção
    print('Erro ao acessar um indice inválido da lista')

except BaseException as ex: #pegando a exceção de base, ou seja, a base das exceções, armazenando o erro em ex
    print('erro desconhecido. Erro: {}'.format(ex))

#BaseException é a primeira das exceções, ou seja, todas as outras estão dentro dela.
#documentação em https://docs.python.org/3/library/exceptions.html
#incluir ao topo sempre os filhos, ou seja, as ultimas exceções.

else: #executa quando não passa pelas exceções
    print('Executa quando não ocorre exceção')

finally: #executa independente se der erro ou não.
    print('Sempre executa')
    print('Fechano arquivo')
    arquivo.close()

