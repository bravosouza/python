#desclarando um método que realiza uma soma
def teste(a, b): #método com dois valores.

#método é o que não tem retorno e função é tudo que tem retorno.
#método nos auxilia nas repetições de funções.

 def soma(a, b):
    return a + b

 def subtracao(a, b):
     return a -b

 def multiplicacao(a, b):
     return a * b

 def divisao(a, b):
     return a / b

#print(soma(1, 2))
#print(subtracao(10, 2))
#print(multiplicacao(5,5))
#print(divisao(10,2))

#declarando uma classe:

class Calculadora: #toda classe começa com letra maiuscula
    def __init__(self, num1, num2): #toda vez que passar pela classe inicia nesse método
        self.valor_a = num1 #instancia o objeto
        self.valor_b = num2

    def soma(self):
        return self.valor_a + self.valor_b

    def subtracao(self):
        return self.valor_a - self.valor_b

    def multiplicacao(self):
        return self.valor_a * self.valor_b

    def divisao(self):
        return self.valor_a / self.valor_b

if __name__ == '__main__':
    calculadora = Calculadora(10, 2)
    print(calculadora.valor_a)
    print(calculadora.valor_b)

    print(calculadora.soma())
    print(calculadora.subtracao())
    print(calculadora.multiplicacao())
    print(calculadora.divisao())


